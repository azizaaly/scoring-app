bool isNumeric(String str) {
  try {
    var value = double.parse(str);
  } on FormatException {
    return false;
  }
  return true;
}
