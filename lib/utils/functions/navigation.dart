import 'package:flutter/cupertino.dart';
import '/utils/constants/routes.dart';

void goToStartGameScreen(context) {
  Navigator.of(context).popAndPushNamed(
    RouteConst.START_GAME_SCREEN,
  );
}

void goBackToStartGameScreen(context) {
  Navigator.of(context).pushNamedAndRemoveUntil(
    RouteConst.START_GAME_SCREEN,
    (Route<dynamic> route) => false,
  );
}

void goToChooseDifficultyLevelScreen(context) {
  Navigator.of(context).pushNamed(
    RouteConst.DIFFICULTY_SCREEN,
  );
}

void goToChooseTimeScreen(
  context, {
  required int difficultyLevel,
}) {
  Navigator.of(context).pushNamed(
    RouteConst.TIME_SCREEN,
    arguments: {
      'difficulty_level': difficultyLevel,
    },
  );
}

void goToGameScreen(
  context, {
  required int difficultyLevel,
  required int time,
}) {
  Navigator.of(context).pushNamed(
    RouteConst.GAME_SCREEN,
    arguments: {
      'difficulty_level': difficultyLevel,
      'time': time,
    },
  );
}

void goToResultsScreen(
  context, {
  required int numOfQuestions,
  required int numOfCorrectAnswers,
}) {
  Navigator.of(context).pushNamed(
    RouteConst.RESULTS_SCREEN,
    arguments: {
      'num_of_questions': numOfQuestions,
      'num_of_correct_answers': numOfCorrectAnswers,
    },
  );
}
