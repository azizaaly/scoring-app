// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

/// basic
const COLOR_WHITE = Color.fromRGBO(255, 255, 255, 1);
const COLOR_BLACK = Color.fromRGBO(0, 0, 0, 1);
const COLOR_GREY = Color.fromRGBO(91, 87, 87, 1);

/// greens
const COLOR_LIGHT_GREEN = Color.fromRGBO(187, 226, 58, 1);
const COLOR_DARK_GREEN = Color.fromRGBO(1, 171, 93, 1);

const COLOR_244_248_247 = Color.fromRGBO(244, 248, 247, 1);
