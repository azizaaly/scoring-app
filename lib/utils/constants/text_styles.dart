// ignore_for_file: constant_identifier_names

import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';

/// 14
const TextStyle lightGreen_InterRegular_14_TextStyle = TextStyle(
  fontFamily: "Inter",
  fontSize: 14,
  color: COLOR_LIGHT_GREEN,
  fontWeight: FontWeight.w400,
);
const TextStyle black_InterREgular_14_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 14,
  color: COLOR_BLACK,
  fontWeight: FontWeight.w400,
);

/// 16
const TextStyle white_InterMedium_16_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 16,
  color: COLOR_WHITE,
  fontWeight: FontWeight.w500,
);
const TextStyle black_InterMedium_16_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 16,
  color: COLOR_BLACK,
  fontWeight: FontWeight.w500,
);
const TextStyle black_InterRegular_16_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 16,
  color: COLOR_BLACK,
  fontWeight: FontWeight.w400,
);
const TextStyle white_InterRegular_16_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 16,
  color: COLOR_WHITE,
  fontWeight: FontWeight.w400,
);
const TextStyle darkGreen_InterRegular_16_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 16,
  color: COLOR_DARK_GREEN,
  fontWeight: FontWeight.w400,
);

/// 18
const TextStyle black_InterRegular_20_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 20,
  color: COLOR_BLACK,
  fontWeight: FontWeight.w400,
);

/// 20
const TextStyle white_InterMedium_20_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 20,
  color: COLOR_WHITE,
  fontWeight: FontWeight.w500,
);

/// 24
const TextStyle darkGreen_MontRegular_24_TextStyle = TextStyle(
  fontFamily: "regular-Mont",
  fontSize: 24,
  color: COLOR_DARK_GREEN,
);
const TextStyle black_InterRegular_24_TextStyle = TextStyle(
  fontFamily: "inter",
  fontSize: 24,
  color: COLOR_BLACK,
  fontWeight: FontWeight.w400,
);

const TextStyle darkGreen_MontRegular_36_TextStyle = TextStyle(
  fontFamily: "regular-Mont",
  fontSize: 36,
  color: COLOR_DARK_GREEN,
);
