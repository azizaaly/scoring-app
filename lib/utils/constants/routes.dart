// ignore_for_file: constant_identifier_names

class RouteConst {
  static const String SPLASH_SCREEN = '/';
  static const String START_GAME_SCREEN = '/start-game';
  static const String DIFFICULTY_SCREEN = '/difficulty-screen';
  static const String TIME_SCREEN = '/time-screen';
  static const String GAME_SCREEN = '/game';
  static const String RESULTS_SCREEN = '/results-screen';
}
