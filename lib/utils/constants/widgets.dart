// ignore_for_file: constant_identifier_names

import 'package:flutter/cupertino.dart';

/// padding
const padding_horizontal_10 = EdgeInsets.symmetric(
  horizontal: 10,
);
const padding_horizontal_16 = EdgeInsets.symmetric(
  horizontal: 16,
);
const padding_bottom_20 = EdgeInsets.only(
  bottom: 20,
);
const padding_bottom_32 = EdgeInsets.only(
  bottom: 32,
);
const padding_horizontal_24_vertical_16 = EdgeInsets.symmetric(
  vertical: 16,
  horizontal: 24,
);
const padding_horizontal_16_vertical_4 = EdgeInsets.symmetric(
  vertical: 4,
  horizontal: 16,
);
const padding_right_12 = EdgeInsets.only(
  right: 12,
);
const padding_top_28 = EdgeInsets.only(
  top: 28,
);

/// height
const vertical_space_8 = SizedBox(
  height: 8,
);
const vertical_space_10 = SizedBox(
  height: 10,
);
const vertical_space_18 = SizedBox(
  height: 18,
);
const vertical_space_28 = SizedBox(
  height: 28,
);
const vertical_space_32 = SizedBox(
  height: 32,
);

const vertical_space_40 = SizedBox(
  height: 40,
);
const vertical_space_80 = SizedBox(
  height: 80,
);
const horizontal_space_8 = SizedBox(
  width: 8,
);
const horizontal_space_10 = SizedBox(
  width: 10,
);
