import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

class SelectItem extends StatelessWidget {
  final String title;
  final bool isSelected;
  final Function select;

  // ignore: use_key_in_widget_constructors
  const SelectItem({
    required this.title,
    required this.isSelected,
    required this.select,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => select(),
      child: Container(
        alignment: Alignment.center,
        padding: padding_horizontal_16_vertical_4,
        margin: padding_right_12,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: isSelected ? COLOR_LIGHT_GREEN : COLOR_244_248_247,
        ),
        child: Text(
          title,
          style: isSelected
              ? white_InterRegular_16_TextStyle
              : black_InterRegular_16_TextStyle,
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}
