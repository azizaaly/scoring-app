import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/text_styles.dart';
import '/utils/constants/widgets.dart';

class Button extends StatelessWidget {
  final Function onTap;
  final String text;

  const Button({
    Key? key,
    required this.onTap,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
        alignment: Alignment.center,
        margin: padding_bottom_20,
        height: 56,
        decoration: BoxDecoration(
          color: COLOR_LIGHT_GREEN,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Center(
          child: Text(
            text,
            style: white_InterMedium_16_TextStyle,
          ),
        ),
      ),
    );
  }
}
