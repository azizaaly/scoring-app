import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

class CustomTextFormField extends StatelessWidget {
  final TextInputType keyboardType;
  final TextEditingController? textController;

  // ignore: use_key_in_widget_constructors
  const CustomTextFormField({
    this.textController,
    this.keyboardType = TextInputType.text,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textController,
      keyboardType: keyboardType,
      style: black_InterREgular_14_TextStyle,
      decoration: InputDecoration(
        fillColor: COLOR_244_248_247,
        floatingLabelStyle: lightGreen_InterRegular_14_TextStyle,
        filled: true,
        alignLabelWithHint: true,
        contentPadding: padding_horizontal_24_vertical_16,
        border: const OutlineInputBorder(),
        labelStyle: black_InterMedium_16_TextStyle,
        focusColor: COLOR_LIGHT_GREEN,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(
            color: COLOR_LIGHT_GREEN,
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(
            color: COLOR_244_248_247,
          ),
        ),
      ),
    );
  }
}
