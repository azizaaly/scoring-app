import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/images.dart';
import '/utils/constants/text_styles.dart';

class CustomAppBar extends StatelessWidget {
  final String text;
  final bool? hasBackButton;

  const CustomAppBar({
    Key? key,
    required this.text,
    this.hasBackButton = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: COLOR_LIGHT_GREEN,
      toolbarHeight: 82,
      title: Text(
        text,
        style: white_InterMedium_20_TextStyle,
        textAlign: TextAlign.start,
        maxLines: 2,
      ),
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(8),
        ),
      ),
      automaticallyImplyLeading: hasBackButton == true,
      leading: hasBackButton == true
          ? IconButton(
              onPressed: () => Navigator.pop(context),
              icon: SvgPicture.asset(
                ARROW_LEFT_LIGHT_ICON,
                color: COLOR_WHITE,
              ),
            )
          : null,
      elevation: 0,
    );
  }
}
