import 'package:flutter/material.dart';

/// constants
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

class Winner extends StatelessWidget {
  final int place;
  final String name;
  final int score;

  // ignore: use_key_in_widget_constructors
  const Winner({
    required this.place,
    required this.name,
    required this.score,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          "$place Place:",
          style: darkGreen_InterRegular_16_TextStyle,
        ),
        horizontal_space_8,
        Text(
          "$name with $score score",
          style: black_InterRegular_16_TextStyle,
        ),
      ],
    );
  }
}
