import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

/// functions
import '/utils/functions/navigation.dart';

/// components
import '/ui/components/essentials/appBar.dart';
import '/ui/components/essentials/button.dart';

class ChooseTimeScreen extends StatelessWidget {
  final int difficultyLevel;

  const ChooseTimeScreen({
    Key? key,
    required this.difficultyLevel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_WHITE,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(82.0),
        child: CustomAppBar(
          text: 'Choose timer',
          hasBackButton: false,
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: padding_horizontal_16,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              vertical_space_80,

              /// form
              Padding(
                padding: padding_horizontal_24_vertical_16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Text(
                      "Choose timer",
                      style: darkGreen_MontRegular_24_TextStyle,
                      textAlign: TextAlign.center,
                    ),
                    vertical_space_18,
                  ],
                ),
              ),

              Button(
                onTap: () => goToGameScreen(
                  context,
                  time: 3,
                  difficultyLevel: difficultyLevel,
                ),
                text: "3 seconds",
              ),
              Button(
                onTap: () => goToGameScreen(
                  context,
                  time: 5,
                  difficultyLevel: difficultyLevel,
                ),
                text: "5 seconds",
              ),
              Button(
                onTap: () => goToGameScreen(
                  context,
                  time: 10,
                  difficultyLevel: difficultyLevel,
                ),
                text: "10 seconds",
              ),
              vertical_space_32,
            ],
          ),
        ),
      ),
    );
  }
}
