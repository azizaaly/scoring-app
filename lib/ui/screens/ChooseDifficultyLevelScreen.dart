import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

/// functions
import '/utils/functions/navigation.dart';

/// components
import '/ui/components/essentials/appBar.dart';
import '/ui/components/essentials/button.dart';

class ChooseDifficultyLevelScreen extends StatelessWidget {
  const ChooseDifficultyLevelScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_WHITE,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(82.0),
        child: CustomAppBar(
          text: 'Choose difficulty level',
          hasBackButton: false,
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: padding_horizontal_16,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              vertical_space_80,

              /// form
              Padding(
                padding: padding_horizontal_24_vertical_16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Text(
                      "Choose difficulty level of questions",
                      style: darkGreen_MontRegular_24_TextStyle,
                      textAlign: TextAlign.center,
                    ),
                    vertical_space_18,
                  ],
                ),
              ),

              Button(
                onTap: () => goToChooseTimeScreen(
                  context,
                  difficultyLevel: 1,
                ),
                text: "Level 1",
              ),
              Button(
                onTap: () => goToChooseTimeScreen(
                  context,
                  difficultyLevel: 2,
                ),
                text: "Level 2",
              ),
              Button(
                onTap: () => goToChooseTimeScreen(
                  context,
                  difficultyLevel: 3,
                ),
                text: "Level 3",
              ),
              vertical_space_32,
            ],
          ),
        ),
      ),
    );
  }
}
