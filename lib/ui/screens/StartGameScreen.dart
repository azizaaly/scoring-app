import 'package:flutter/material.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

/// functions
import '/utils/functions/navigation.dart';

/// components
import '/ui/components/essentials/appBar.dart';
import '/ui/components/essentials/button.dart';

class StartGameScreen extends StatelessWidget {
  const StartGameScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_WHITE,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(82.0),
        child: CustomAppBar(
          text: 'Start Game',
          hasBackButton: false,
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: padding_horizontal_16,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              /// form
              vertical_space_80,
              Padding(
                padding: padding_horizontal_24_vertical_16,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Text(
                      "Are you ready to start your game ?",
                      style: darkGreen_MontRegular_24_TextStyle,
                      textAlign: TextAlign.center,
                    ),
                    vertical_space_18,
                    Text(
                      "You will be given math questions. Try your best and get the highest result !",
                      style: black_InterRegular_16_TextStyle,
                      textAlign: TextAlign.center,
                    ),
                    vertical_space_18,
                  ],
                ),
              ),

              /// button
              Button(
                onTap: () => goToChooseDifficultyLevelScreen(context),
                text: "Start",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
