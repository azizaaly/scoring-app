import 'dart:async';
import 'package:flutter/material.dart';
import 'package:scoring_app/data/questions.dart';

/// constants
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

/// functions
import '/utils/functions/navigation.dart';

/// models
import '/data/models/question.dart';

/// components
import '/ui/components/essentials/appBar.dart';
import '/ui/components/essentials/button.dart';
import '/ui/components/essentials/customTextFormField.dart';

class GameScreen extends StatefulWidget {
  final int difficultyLevel;
  final int time;

  const GameScreen({
    Key? key,
    required this.difficultyLevel,
    required this.time,
  }) : super(key: key);

  @override
  State<GameScreen> createState() => _GameScreenState();
}

class _GameScreenState extends State<GameScreen> {
  late TextEditingController controller;

  late List<Question> questions = [];
  var index = 0;
  int numOfCorrectQuestions = 0;

  Duration duration = const Duration();
  Timer? timer;
  bool countDown = true;

  void next(context) {
    if (questions[index].answer == controller.text) {
      questions[index].isCorrect = true;
    } else {
      questions[index].isCorrect = false;
    }

    if (index == questions.length - 1) {
      finish(context);
    } else {
      setState(() {
        index++;
      });
      controller.text = "";
      FocusManager.instance.primaryFocus?.unfocus();
      reset(Duration(seconds: widget.time));
    }
  }

  void checkResults() {
    for (var i = 0; i < questions.length; i++) {
      if (questions[i].isCorrect) {
        numOfCorrectQuestions++;
      }
    }
  }

  void finish(context) {
    checkResults();
    goToResultsScreen(
      context,
      numOfQuestions: questions.length,
      numOfCorrectAnswers: numOfCorrectQuestions,
    );
  }

  @override
  void initState() {
    if (widget.difficultyLevel == 1) {
      questions = levelOneQuestions;
    } else if (widget.difficultyLevel == 2) {
      questions = levelTwoQuestions;
    } else {
      questions = levelThreeQuestions;
    }
    reset(Duration(seconds: widget.time));
    startTimer();
    controller = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    if (timer != null && timer!.isActive) timer!.cancel();
    super.dispose();
  }

  void reset(countDownDuration) {
    if (countDown) {
      setState(() => duration = countDownDuration);
    } else {
      setState(() => duration = const Duration());
    }
  }

  void startTimer() {
    timer = Timer.periodic(const Duration(seconds: 1), (_) => addTime());
  }

  void addTime() {
    final addSeconds = countDown ? -1 : 1;
    if (timer!.isActive) {
      setState(() {
        final seconds = duration.inSeconds + addSeconds;
        if (seconds < 0) {
          timer?.cancel();
          next(context);
        } else {
          duration = Duration(seconds: seconds);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    final seconds = twoDigits(duration.inSeconds.remainder(60));

    return Scaffold(
      backgroundColor: COLOR_WHITE,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(82.0),
        child: CustomAppBar(
          text: 'Game',
          hasBackButton: false,
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: Padding(
          padding: padding_horizontal_16,
          child: Stack(
            children: [
              Container(
                alignment: Alignment.topCenter,
                margin: padding_top_28,
                child: Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: const Color.fromARGB(255, 154, 191, 255),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Text(
                    "Time left: $seconds s",
                    style: black_InterRegular_20_TextStyle,
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  /// form
                  Padding(
                    padding: padding_horizontal_24_vertical_16,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          questions[index].question,
                          style: darkGreen_MontRegular_24_TextStyle,
                          textAlign: TextAlign.center,
                        ),
                        vertical_space_18,
                        CustomTextFormField(
                          keyboardType: TextInputType.number,
                          textController: controller,
                        ),
                        vertical_space_18,
                      ],
                    ),
                  ),

                  /// button
                  Button(
                    onTap: () => next(context),
                    text: index == questions.length - 1 ? "Finish" : "Next",
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
