import 'package:flutter/material.dart';

/// constants
import '/utils/constants/images.dart';
import '/utils/constants/colors.dart';
import '/utils/constants/widgets.dart';
import '/utils/constants/text_styles.dart';

/// functions
import '/utils/functions/navigation.dart';

/// components
import '/ui/components/essentials/appBar.dart';
import '/ui/components/essentials/button.dart';

class ResultsScreen extends StatelessWidget {
  final int numOfQuestions;
  final int numOfCorrectAnswers;

  const ResultsScreen({
    Key? key,
    required this.numOfQuestions,
    required this.numOfCorrectAnswers,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: COLOR_WHITE,
      appBar: const PreferredSize(
        preferredSize: Size.fromHeight(82.0),
        child: CustomAppBar(
          text: "Results",
          hasBackButton: false,
        ),
      ),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
          padding: padding_horizontal_16,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              vertical_space_80,
              Image.asset(
                RESULT_IMG,
                width: 180,
                height: 180,
              ),
              vertical_space_32,
              Text(
                "You got $numOfCorrectAnswers correct answers from $numOfQuestions questions",
                style: black_InterRegular_20_TextStyle,
                textAlign: TextAlign.center,
              ),
              vertical_space_18,
              Text(
                numOfCorrectAnswers >= numOfQuestions - 2
                    ? "You did great job. Continue achieving high results !"
                    : (numOfCorrectAnswers >= numOfQuestions - 5
                        ? "You did good job. Next time try harder to achieve higher results"
                        : "You have scored relatively low than you peers. Learn more"),
                style: black_InterRegular_16_TextStyle,
                textAlign: TextAlign.center,
              ),
              vertical_space_40,
              Button(
                onTap: () => goBackToStartGameScreen(context),
                text: "Restart",
              ),
            ],
          ),
        ),
      ),
    );
  }
}
