class Question {
  final String question;
  final String answer;
  bool isCorrect;

  Question({
    required this.question,
    required this.answer,
    this.isCorrect = false,
  });
}
