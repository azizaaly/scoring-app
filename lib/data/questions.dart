import 'models/question.dart';

final levelOneQuestions = [
  Question(question: "5+4=", answer: "9"),
  Question(question: "12-5=", answer: "7"),
  Question(question: "8+4=", answer: "12"),
  Question(question: "3+6=", answer: "9"),
  Question(question: "10-4=", answer: "6"),
  Question(question: "8-4=", answer: "4"),
  Question(question: "2+3=", answer: "5"),
  Question(question: "2+4=", answer: "6"),
  Question(question: "6-5=", answer: "1"),
  Question(question: "5+7=", answer: "12"),
];

final levelTwoQuestions = [
  Question(question: "2*3=", answer: "6"),
  Question(question: "4*2=", answer: "8"),
  Question(question: "0*5=", answer: "0"),
  Question(question: "1*8=", answer: "8"),
  Question(question: "4*3=", answer: "12"),
  Question(question: "2*10=", answer: "20"),
  Question(question: "8*2=", answer: "16"),
  Question(question: "9*1=", answer: "9"),
  Question(question: "7*2=", answer: "14"),
  Question(question: "5*2=", answer: "10"),
];

final levelThreeQuestions = [
  Question(question: "(5 + 4) * 2 =", answer: "18"),
  Question(question: "2 * 10 - 20 =", answer: "0"),
  Question(question: "5 + 6 -12 =", answer: "-1"),
  Question(question: "8 + 12 * 2 =", answer: "32"),
  Question(question: "10 - 4 + 8 =", answer: "14"),
  Question(question: "(8 - 4) * 0 =", answer: "0"),
  Question(question: "2 + 3 * 4 =", answer: "14"),
  Question(question: "3 * 5 - 12 =", answer: "3"),
  Question(question: "20 - 3 * 6 =", answer: "2"),
  Question(question: "10 * 2 / 4=", answer: "5"),
];
