import 'package:flutter/material.dart';

/// constants
import '/utils/constants/routes.dart';

/// screens
import '/ui/screens/GameScreen.dart';
import '/ui/screens/SplashScreen.dart';
import '/ui/screens/ResultsScreen.dart';
import '/ui/screens/StartGameScreen.dart';
import '/ui/screens/ChooseTimeScreen.dart';
import '/ui/screens/ChooseDifficultyLevelScreen.dart';

class AppRouter {
  Route generateRoute(RouteSettings settings) {
    switch (settings.name) {

      /// splash screen
      case RouteConst.SPLASH_SCREEN:
        return MaterialPageRoute(
          builder: (_) => const SplashScreen(),
        );

      /// start game
      case RouteConst.START_GAME_SCREEN:
        return MaterialPageRoute(
          builder: (_) => const StartGameScreen(),
        );

      /// difficulty level
      case RouteConst.DIFFICULTY_SCREEN:
        return MaterialPageRoute(
          builder: (_) => const ChooseDifficultyLevelScreen(),
        );

      /// time
      case RouteConst.TIME_SCREEN:
        final data = settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
          builder: (_) => ChooseTimeScreen(
            difficultyLevel: data['difficulty_level'],
          ),
        );

      /// game sceen
      case RouteConst.GAME_SCREEN:
        final data = settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
          builder: (_) => GameScreen(
            difficultyLevel: data['difficulty_level'],
            time: data['time'],
          ),
        );

      /// results
      case RouteConst.RESULTS_SCREEN:
        final data = settings.arguments as Map<String, dynamic>;
        return MaterialPageRoute(
          builder: (_) => ResultsScreen(
            numOfQuestions: data['num_of_questions'],
            numOfCorrectAnswers: data['num_of_correct_answers'],
          ),
        );

      /// default
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
