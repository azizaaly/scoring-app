import 'package:flutter/material.dart';
import 'package:scoring_app/utils/constants/routes.dart';
import 'generateRoute.dart';

void main() {
  runApp(
    MyApp(
      router: AppRouter(),
    ),
  );
}

class MyApp extends StatefulWidget {
  final AppRouter router;
  const MyApp({
    Key? key,
    required this.router,
  }) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => navigatorKey.currentState!.pushNamed(
        RouteConst.DIFFICULTY_SCREEN,
      ),
      child: MaterialApp(
        navigatorKey: navigatorKey,
        debugShowCheckedModeBanner: false,
        onGenerateRoute: widget.router.generateRoute,
        title: 'Scoring app',
        initialRoute: RouteConst.SPLASH_SCREEN,
      ),
    );
  }
}
